# OCI_Servers

Instructions, scripts, yaml, etc... for deploying Open Cloud Initiative Matrix servers.

To register a user from the matrix container:
`podman exec matrix register_new_matrix_user -u USERNAME -p "Replace all within quotes" -c /data/homeserver.yaml http://127.0.0.1:8008`

The command requires "http://" and "8008" is the matrix port.
In addition, one can add `--admin` or `--no-admin`.
