#!/bin/sh

podman pod create \
    --network proxy-net \
    --name pod-matrix

# The database
podman run --pod pod-matrix \
    --detach \
    --name matrix-db \
    --read-only \
    --volume matrix-db:/var/lib/postgresql/data:Z \
    --env POSTGRES_USER=synapse_user \
    --env POSTGRES_DB=synapse \
    --env POSTGRES_INITDB_ARGS="--encoding='UTF8' --lc-collate='C' --lc-ctype='C'" \
    --env POSTGRES_PASSWORD=$MATRIX_POSTGRES_PASSWORD\
    docker.io/library/postgres:13

# Generate matrix's config files
podman run -it --rm \
    --volume matrix-data:/data:Z \
    --read-only \
    --env SYNAPSE_SERVER_NAME=matrix.$DOMAIN \
    --env SYNAPSE_REPORT_STATS=no \
    docker.io/matrixdotorg/synapse:latest generate

# Matrix/Synapse server
podman run --detach \
    --name matrix \
    --read-only \
    --env SYNAPSE_SERVER_NAME=matrix.$DOMAIN \
    --volume matrix-data:/data:Z \
    --pod pod-matrix \
    docker.io/matrixdotorg/synapse:latest

# Matrix Web Client
podman run --name element-web \
    --mount type=bind,source=${HOME}/.local/share/containers/storage/volumes/matrix_config.json,destination=/etc/element-web/config.json,ro,relabel=private \
    --pod pod-matrix \
    --detach \
    docker.io/vectorim/element-web


